import exceptions.PilaBuidaException;

import java.util.*;

public class Navegador {

    private Pila<String> endavant = new Pila<>();
    private Pila<String> enrere = new Pila<>();
    private List<String> historial = new ArrayList<>();
    private TreeMap<String,Integer> numVisites = new TreeMap<String,Integer>();

    public Navegador(String s) {
        this.anarA(s);
    }


    public void anarA(String novaURL) {
        if(historial.isEmpty()) {
            historial.add(novaURL);
        }
        else {
            historial.add(novaURL);
            System.out.println(novaURL);
        }

        enrere.push(novaURL);

        if(!numVisites.containsKey(novaURL)) {
            numVisites.put(novaURL, 1);
        }else {
            int visites = numVisites.get(novaURL);
            numVisites.replace(novaURL,visites, visites+1);
        }
    }

    public void enrere() {
        try {
            endavant.push(enrere.pop());

            System.out.println(enrere.peek());

        }catch (PilaBuidaException e) { }
    }

    public void endavant() {
        try {
            System.out.println(endavant.peek());

            enrere.push(endavant.pop());

        } catch(PilaBuidaException e) {}
    }

    public void veureHistorial() {
        for(String p: historial){
            System.out.println(p);
        }

    }

    public void veureVisitades() {
        Comparator<String> comparator = new OrdenarNumVisites(numVisites);
        TreeMap<String, Integer> visitesSort = new TreeMap<>(comparator);
        visitesSort.putAll(numVisites);

        for(Map.Entry<String, Integer> vis : visitesSort.entrySet()) {
            System.out.println(vis);
        }
    }


}
