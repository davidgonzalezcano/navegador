import exceptions.PilaBuidaException;

import java.util.ArrayDeque;
import java.util.Iterator;

public class Pila<E> implements Iterable{

    private ArrayDeque<E> pila = new ArrayDeque<>();

    @Override
    public Iterator<E> iterator() {
        return pila.iterator();
    }

    public boolean empty() {
        if(pila.isEmpty()) return true;
        else return false;
    }

    public E peek() throws PilaBuidaException {
        if(pila.isEmpty()) {
            throw new PilaBuidaException();
        } else {
            return pila.peek();
        }
    }

    public E pop() throws PilaBuidaException{
        if(pila.isEmpty()) {
            throw new PilaBuidaException();
        } else {
            return pila.pop();
        }
    }

    public void push(E e) {
        pila.push(e);
    }

    public void removeAllElements(){
        this.pila.clear();
    }

}
