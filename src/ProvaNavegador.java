import java.util.Scanner;

public class ProvaNavegador {
    public static void main(String[] args) {

        Navegador navegador = new Navegador("http://www.itb.cat");

        menu();

        boolean endProgram = false;

        while (!endProgram) {


            int optionSelected = options();

            switch(optionSelected) {
                case 1:
                    System.out.println("Introdueix una URL");
                    Scanner scanner = new Scanner(System.in);
                    String URL = scanner.nextLine();
                    navegador.anarA(URL);
                    break;
                case 2:
                    navegador.enrere();
                    break;
                case 3:
                    navegador.endavant();
                    break;
                case 4:
                    navegador.veureHistorial();
                    break;
                case 5:
                    navegador.veureVisitades();
                    break;
                case 6:
                    GUI_Navegador GUI_Navegador = new GUI_Navegador();
                    break;
                case 0:
                    System.out.println("Fi del programa.");
                    endProgram = true;
                    break;
                default:
                    System.out.println("opció incorrecta, torna a intentar-ho:");
            }
        }


    }




    public static void menu() {
        System.out.println("MENU DEL NAVEGADOR");
        System.out.println("1.Introduir URL.");
        System.out.println("2.Anar endavant.");
        System.out.println("3.Anar enrere.");
        System.out.println("4.Veure historial.");
        System.out.println("5.Veure les visites.");
        System.out.println("6. GUI navegador.");
        System.out.println("0.Sortir del programa.");

    }

    public static int options() {
        int optionSelected = -1;

        do {
            System.out.println("Escull una opció:");

            Scanner scanner = new Scanner(System.in);
            String optionText = scanner.nextLine();

            if (isInteger(optionText) == true) {
                optionSelected = Integer.parseInt(optionText);

                if(optionSelected < 0 || optionSelected > 6) {
                    System.out.println("No es un nùmero entre 0 i 6.");
                    optionSelected = -1;
                }
            } else {
                System.out.println("No es un nùmero correcte.");
            }

        } while(optionSelected < 0);

        return optionSelected;
    }


    public static boolean isInteger(String num) {
        boolean result;

        try {
            Integer.parseInt(num);
            result = true;

        } catch (NumberFormatException excepcion) {
            result = false;
        }

        return result;
    }
}