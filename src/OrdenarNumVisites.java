import java.util.Comparator;
import java.util.TreeMap;

public class OrdenarNumVisites  implements Comparator<String> {

    TreeMap<String, Integer> numVisitesOrdenat = new TreeMap<>();

    public OrdenarNumVisites(TreeMap<String, Integer> numVisitesOrdenat) {
        this.numVisitesOrdenat.putAll(numVisitesOrdenat);
    }

    @Override
    public int compare(String s1, String s2) {
        if(numVisitesOrdenat.get(s1) >= numVisitesOrdenat.get(s2)) {
            return -1;
        }else {
            return 1;
        }
    }
}
